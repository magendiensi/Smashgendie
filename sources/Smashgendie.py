import pygame
import sys
from random import randint


# Initialisation de Pygame
pygame.init()

# Paramètres du jeu
largeur_fenetre = 1200
hauteur_fenetre = 650

# Couleurs
blanc = (255, 255, 255)
bleu = (0, 0, 255)
rouge = (255, 0, 0)
noir = (0, 0, 0)

# Création de la fenêtre
fenetre = pygame.display.set_mode((largeur_fenetre, hauteur_fenetre))
pygame.display.set_caption("Smashgendie")

# Charger les images de fond
fond = pygame.image.load("fond_volley.png")
fond = pygame.transform.scale(fond, (largeur_fenetre, hauteur_fenetre))

fond_accueil = pygame.image.load("fond_accueil.png")
fond_accueil = pygame.transform.scale(fond_accueil, (largeur_fenetre, hauteur_fenetre))

titre_accueil = pygame.image.load("smashgendie_titre.png")
titre_accueil = pygame.transform.scale_by(titre_accueil, (0.6, 0.6))

# Personnages
personnage_taille = 100
personnage_vitesse = 8
saut_compteur_max = 15

# Joueur 1 (Gauche)
joueur1_x = largeur_fenetre // 4 - personnage_taille // 2
joueur1_y = hauteur_fenetre - personnage_taille - 10
saut_joueur1 = False
saut_compteur_joueur1 = saut_compteur_max


sautG = pygame.image.load("persoGauche_saut.png")
sautG = pygame.transform.scale(sautG, (personnage_taille, personnage_taille))

attente1 = pygame.image.load("persoGauche_attente.png")
attente1 = pygame.transform.scale(attente1, (personnage_taille, personnage_taille))

piedDroit1 = pygame.image.load("persoGauche_piedDroit.png")
piedDroit1 = pygame.transform.scale(piedDroit1, (personnage_taille, personnage_taille))

piedGauche1 = pygame.image.load("persoGauche_piedGauche.png")
piedGauche1 = pygame.transform.scale(piedGauche1, (personnage_taille, personnage_taille))

frappe1 = pygame.image.load("persoGauche_smash.png")
frappe1 = pygame.transform.scale(frappe1, (personnage_taille, personnage_taille))


# Joueur 2 (Droite)
joueur2_x = 3 * largeur_fenetre // 4 - personnage_taille // 2
joueur2_y = hauteur_fenetre - personnage_taille - 10
saut_joueur2 = False
saut_compteur_joueur2 = saut_compteur_max

sautD = pygame.image.load("persoDroite_saut.png")
sautD = pygame.transform.scale(sautD, (personnage_taille, personnage_taille))

attente2 = pygame.image.load("persoDroite_attente.png")
attente2 = pygame.transform.scale(attente2, (personnage_taille, personnage_taille))

piedDroit2 = pygame.image.load("persoDroite_piedDroit.png")
piedDroit2 = pygame.transform.scale(piedDroit2, (personnage_taille, personnage_taille))

piedGauche2 = pygame.image.load("persoDroite_piedGauche.png")
piedGauche2 = pygame.transform.scale(piedGauche2, (personnage_taille, personnage_taille))

frappe2 = pygame.image.load("persoDroite_smash.png")
frappe2 = pygame.transform.scale(frappe2, (personnage_taille, personnage_taille))



# Bouton play
#50*21
bouton_play = pygame.image.load("Play.png")
bouton_play = pygame.transform.scale(bouton_play, (300,200))


# Balle
balle_rayon = 25
balle_coter_x = 35
balle_coter_y = 35
balle_x = largeur_fenetre // 2
balle_y = hauteur_fenetre // 4
balle_vitesse_x = 5
balle_vitesse_y = 5
gravite = 0.5
frottement = 0.99

balle1 = pygame.image.load("Ballon_Volley_1.png")
balle1 = pygame.transform.scale(balle1, (balle_coter_x, balle_coter_y))

balle2 = pygame.image.load("Ballon_Volley_2.png")
balle2 = pygame.transform.scale(balle2, (balle_coter_x, balle_coter_y))

balle3 = pygame.image.load("Ballon_Volley_3.png")
balle3 = pygame.transform.scale(balle3, (balle_coter_x, balle_coter_y))

balle4 = pygame.image.load("Ballon_Volley_4.png")
balle4 = pygame.transform.scale(balle4, (balle_coter_x, balle_coter_y))

# Numéros score
zero_R = pygame.image.load("0R.png")
zero_R = pygame.transform.scale(zero_R, (50, 50))
zero_B = pygame.image.load("0B.png")
zero_B = pygame.transform.scale(zero_B, (50, 50))

un_R = pygame.image.load("1R.png")
un_R = pygame.transform.scale(un_R, (50, 50))
un_B = pygame.image.load("1B.png")
un_B = pygame.transform.scale(un_B, (50, 50))

deux_R = pygame.image.load("2R.png")
deux_R = pygame.transform.scale(deux_R, (50, 50))
deux_B = pygame.image.load("2B.png")
deux_B = pygame.transform.scale(deux_B, (50, 50))

trois_R = pygame.image.load("3R.png")
trois_R = pygame.transform.scale(trois_R, (50, 50))
trois_B = pygame.image.load("3B.png")
trois_B = pygame.transform.scale(trois_B, (50, 50))

quatre_R = pygame.image.load("4R.png")
quatre_R = pygame.transform.scale(quatre_R, (50, 50))
quatre_B = pygame.image.load("4B.png")
quatre_B = pygame.transform.scale(quatre_B, (50, 50))

cinq_R = pygame.image.load("5R.png")
cinq_R = pygame.transform.scale(cinq_R, (50, 50))
cinq_B = pygame.image.load("5B.png")
cinq_B = pygame.transform.scale(cinq_B, (50, 50))

six_R = pygame.image.load("6R.png")
six_R = pygame.transform.scale(six_R, (50, 50))
six_B = pygame.image.load("6B.png")
six_B = pygame.transform.scale(six_B, (50, 50))

sept_R = pygame.image.load("7R.png")
sept_R = pygame.transform.scale(sept_R, (50, 50))
sept_B = pygame.image.load("7B.png")
sept_B = pygame.transform.scale(sept_B, (50, 50))

huit_R = pygame.image.load("8R.png")
huit_R = pygame.transform.scale(huit_R, (50, 50))
huit_B = pygame.image.load("8B.png")
huit_B = pygame.transform.scale(huit_B, (50, 50))

neuf_R = pygame.image.load("9R.png")
neuf_R = pygame.transform.scale(neuf_R, (50, 50))
neuf_B = pygame.image.load("9B.png")
neuf_B = pygame.transform.scale(neuf_B, (50, 50))


# Pièces

face = pygame.image.load("Face-Bleu.png")
face = pygame.transform.scale_by(face, (0.5, 0.5))
pile = pygame.image.load("Pile-Rouge.png")
pile = pygame.transform.scale_by(pile, (0.5, 0.5))

B_commence = pygame.image.load("J-bleu-commence.png")
B_commence = pygame.transform.scale_by(B_commence, (0.4, 0.4))
R_commence = pygame.image.load("J-rouge-commence.png")
R_commence = pygame.transform.scale_by(R_commence, (0.4, 0.4))

face_ballon = pygame.image.load("Piece_ballon.png")
face_ballon = pygame.transform.scale_by(face_ballon, (0.4, 0.4))
face_ballon_2 = pygame.image.load("Piece_ballon.png")
face_ballon_2 = pygame.transform.scale_by(face_ballon_2, (0.5, 0.5))
pile_laurier = pygame.image.load("Piece_laurier.png")
pile_laurier = pygame.transform.scale_by(pile_laurier, (0.4, 0.4))
pile_laurier_2 = pygame.image.load("Piece_laurier.png")
pile_laurier_2 = pygame.transform.scale_by(pile_laurier_2, (0.5, 0.5))

piece_un = pygame.image.load("Piece1.png")
piece_un = pygame.transform.scale_by(piece_un, (0.5, 0.5))
piece_deux = pygame.image.load("Piece2.png")
piece_deux = pygame.transform.scale_by(piece_deux, (0.5, 0.5))
piece_trois = pygame.image.load("Piece3.png")
piece_trois = pygame.transform.scale_by(piece_trois, (0.5, 0.5))
piece_quatre = pygame.image.load("Piece4.png")
piece_quatre = pygame.transform.scale_by(piece_quatre, (0.5, 0.5))
piece_cinq = pygame.image.load("Piece5.png")
piece_cinq = pygame.transform.scale_by(piece_cinq, (0.5, 0.5))
piece_six = pygame.image.load("Piece6.png")
piece_six = pygame.transform.scale_by(piece_six, (0.5, 0.5))
piece_sept = pygame.image.load("Piece7.png")
piece_sept = pygame.transform.scale_by(piece_sept, (0.5, 0.5))
piece_huit = pygame.image.load("Piece8.png")
piece_huit = pygame.transform.scale_by(piece_huit, (0.5, 0.5))
piece_neuf = pygame.image.load("Piece9.png")
piece_neuf = pygame.transform.scale_by(piece_neuf, (0.5, 0.5))

compteur_temps = 650



# Images fautes

quatre_touches_B = pygame.image.load("4-touches-B.png")
quatre_touches_B = pygame.transform.scale_by(quatre_touches_B, (0.5, 0.5))
quatre_touches_R = pygame.image.load("4-touches-R.png")
quatre_touches_R = pygame.transform.scale_by(quatre_touches_R, (0.5, 0.5))

Out_B = pygame.image.load("Out-B.png")
Out_B = pygame.transform.scale_by(Out_B, (0.6, 0.6))
Out_R = pygame.image.load("Out-R.png")
Out_R = pygame.transform.scale_by(Out_R, (0.6, 0.6))

super_spike_B = pygame.image.load("SuperSpike-B.png")
super_spike_B = pygame.transform.scale_by(super_spike_B, (0.5, 0.5))
super_spike_R = pygame.image.load("SuperSpike-R.png")
super_spike_R = pygame.transform.scale_by(super_spike_R, (0.5, 0.5))

point_B = pygame.image.load("Point-B.png")
point_B = pygame.transform.scale_by(point_B, (0.6, 0.6))
point_R = pygame.image.load("Point-R.png")
point_R = pygame.transform.scale_by(point_R, (0.6, 0.6))

victoire_B = pygame.image.load("Victoire-B.png")
victoire_B = pygame.transform.scale_by(victoire_B, (0.4, 0.4))
victoire_R = pygame.image.load("Victoire-R.png")
victoire_R = pygame.transform.scale_by(victoire_R, (0.4, 0.4))


compteur_fautes = 1
compteur_fautes_max = 150
faute = ""
attaque = False

# Filet
filet_largeur = 5
filet_hauteur = 308  
filet_x = largeur_fenetre // 2 - filet_largeur // 2


score_B = 0
score_R = 0
compteur_touches_R = 0
compteur_touches_B = 0
service = randint(1,2)

compteur_victoire = 200
 
touche_j1 = 0
touche_j2 = 0

lancer = False
start = False


# Boucle de jeu
while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()
    
    
    souris_x = pygame.mouse.get_pos()[0]
    souris_y = pygame.mouse.get_pos()[1]
    souris_appuyee = pygame.mouse.get_pressed()[0]
    
    if not(start):
        if not(600-largeur_fenetre//8 < souris_x < 900-largeur_fenetre//8 and 400 < souris_y < 600 and souris_appuyee):

            fenetre.blit(fond_accueil, (0, 0))
            
            fenetre.blit(bouton_play, (600-largeur_fenetre//8, 400)) 
            
            fenetre.blit(titre_accueil, (largeur_fenetre // 2 - titre_accueil.get_width()//2, 150 ))
            
            pygame.display.flip()
        
       
        else:
            start = True
            lancer = True
            
               
    if lancer and start:
        fenetre.blit(fond_accueil, (0, 0))
        if compteur_temps >= 410:
            fenetre.blit(face, (-50, - 150))
            fenetre.blit(pile, (largeur_fenetre//2, -150))
            fenetre.blit(face_ballon, (100, 250))
            fenetre.blit(pile_laurier, (775, 250))
        
        elif 410 > compteur_temps > 350:
            fenetre.blit(fond_accueil, (0, 0))
            hauteur_piece = (compteur_temps - 380)**2 // 3
            
            
            if compteur_temps % 12 == 1:
                fenetre.blit(piece_un, (400, hauteur_piece))
            elif compteur_temps % 12 == 2:
                fenetre.blit(piece_deux, (400, hauteur_piece))
            elif compteur_temps % 12 == 3:
                fenetre.blit(piece_trois, (400, hauteur_piece))
            elif compteur_temps % 12 == 4:
                fenetre.blit(piece_quatre, (400, hauteur_piece))
            elif compteur_temps % 12 == 5:
                fenetre.blit(piece_cinq, (400, hauteur_piece))
            elif compteur_temps % 12 == 6:
                fenetre.blit(piece_six, (400, hauteur_piece))
            elif compteur_temps % 12 == 7:
                fenetre.blit(piece_sept, (400, hauteur_piece))
            elif compteur_temps % 12 == 8:
                fenetre.blit(piece_huit, (400, hauteur_piece))
            elif compteur_temps % 12 == 9:
                fenetre.blit(piece_neuf, (400, hauteur_piece))
            elif compteur_temps % 12 == 10:
                fenetre.blit(piece_quatre, (400, hauteur_piece))
            elif compteur_temps % 12 == 11:
                fenetre.blit(piece_trois, (400, hauteur_piece))
            else:
                fenetre.blit(piece_deux, (400, hauteur_piece))
            pygame.display.flip()
            pygame.time.wait(15)
            
            
            

        else:
            
            if service == 1:
                fenetre.blit(face_ballon_2, (largeur_fenetre // 2 - face_ballon_2.get_width()//2, 200))
                fenetre.blit(B_commence, (100, 0))
            
            else:
                fenetre.blit(pile_laurier_2, (largeur_fenetre // 2 - pile_laurier_2.get_width()//2, 200))
                fenetre.blit(R_commence, (30, 0))
                
        pygame.display.flip()
            
        if compteur_temps == 0:
            lancer = False
        
        compteur_temps -= 1

        
            
            
            
    if start and not(lancer):
        
        if compteur_fautes == 1 :
            joueur1_x = largeur_fenetre // 4 - personnage_taille // 2
            joueur2_x = 3 * largeur_fenetre // 4 - personnage_taille // 2
            if service == 1:
                balle_x = largeur_fenetre // 4 - balle_rayon // 2
            else:
                balle_x = 3 * largeur_fenetre // 4 - balle_rayon // 2
            balle_y = hauteur_fenetre // 3
            balle_vitesse_x = 0
            balle_vitesse_y = -10
            compteur_touches_B = 0
            compteur_touches_R = 0
            saut_joueur1 = False
            saut_compteur_joueur1 = saut_compteur_max
            joueur1_y = hauteur_fenetre - personnage_taille - 10
            saut_joueur2 = False
            saut_compteur_joueur2 = saut_compteur_max
            joueur2_y = hauteur_fenetre - personnage_taille - 10
            attaque = False
            compteur_fautes = 0
        
    
    
        # Gestion des mouvements du joueur 1
        touches_joueur1 = pygame.key.get_pressed()
        if touches_joueur1[pygame.K_q] and joueur1_x > 0:
            joueur1_x -= personnage_vitesse
        if touches_joueur1[pygame.K_d] and joueur1_x < largeur_fenetre // 2 - 5 * personnage_taille // 4:
            joueur1_x += personnage_vitesse

        # Gestion du saut pour le joueur 1
        if not saut_joueur1:
            if touches_joueur1[pygame.K_z]:
                saut_joueur1 = True

        else:
            if saut_compteur_joueur1 >= -saut_compteur_max:
                joueur1_y -= saut_compteur_joueur1 * 3
                saut_compteur_joueur1 -= 1
            else:
                saut_joueur1 = False
                saut_compteur_joueur1 = saut_compteur_max

        # Gestion des mouvements du joueur 2
        touches_joueur2 = pygame.key.get_pressed()
        if touches_joueur2[pygame.K_RIGHT] and joueur2_x < largeur_fenetre - personnage_taille:
            joueur2_x += personnage_vitesse
        if touches_joueur2[pygame.K_LEFT] and joueur2_x > largeur_fenetre // 2 + personnage_taille // 4:
            joueur2_x -= personnage_vitesse

        # Gestion du saut pour le joueur 2
        if not saut_joueur2:
            if touches_joueur2[pygame.K_UP]:
                saut_joueur2 = True
        else:
            if saut_compteur_joueur2 >= -saut_compteur_max:
                joueur2_y -= saut_compteur_joueur2 * 3
                saut_compteur_joueur2 -= 1
            else:
                saut_joueur2 = False
                saut_compteur_joueur2 = saut_compteur_max




        # Collision avec les joueurs
    
        if (
            joueur1_x < balle_x < joueur1_x + personnage_taille and
            joueur1_y < balle_y < joueur1_y + personnage_taille):
        
            
            # Attaque
            if saut_joueur1:
                balle_vitesse_x += 12
                balle_vitesse_y = (joueur1_x - largeur_fenetre // 4 + personnage_taille // 4) // 15
                if compteur_fautes == 0:
                    attaque = True
                
                
            # Réception
            else :
                balle_vitesse_x = (largeur_fenetre // 2 - joueur1_x - personnage_taille // 2) // 60
                balle_vitesse_y = -10 -abs(balle_vitesse_y) // 2
                if compteur_fautes == 0:
                    attaque = False
               
            if compteur_fautes == 0:  
              
                if touche_j1 == 0: 
                    compteur_touches_B += 1
                    touche_j1 = 5
            
                
                compteur_touches_R = 0
                if compteur_touches_B == 4:
                    score_R += 1
                
                    faute = "4 touches B"
                    compteur_fautes = compteur_fautes_max
                
                    service = 2
                
        
        if touche_j1 != 0 :
            touche_j1 -= 1
                      
    
    
        if (
            joueur2_x < balle_x < joueur2_x + personnage_taille and
            joueur2_y < balle_y < joueur2_y + personnage_taille):
            
            
            
            # Attaque
            if saut_joueur2:
                balle_vitesse_x -= 12
                balle_vitesse_y = (3 * largeur_fenetre // 4 - joueur2_x) // 15
                if compteur_fautes == 0:
                    attaque = True
                
                
            # Réception
            else:
                balle_vitesse_x = (largeur_fenetre // 2 - joueur2_x) // 60
                balle_vitesse_y = -10 -abs(balle_vitesse_y) // 2
                if compteur_fautes == 0:
                    attaque = False
                
               
            if compteur_fautes == 0:   
                
                if touche_j2 == 0: 
                    compteur_touches_R += 1
                    touche_j2 = 5
            
            
    
                compteur_touches_B = 0
                if compteur_touches_R == 4:
                    score_B += 1
                
                    faute = "4 touches R"
                    compteur_fautes = compteur_fautes_max
                
                    service = 1
           
        if touche_j2 != 0 :
            touche_j2 -= 1



        # Mise à jour de la position de la balle avec gravité et frottement
        balle_vitesse_y += gravite
        balle_vitesse_x *= frottement
        balle_x += balle_vitesse_x
        balle_y += balle_vitesse_y


        # Rebondissement de la balle sur les bords de la fenêtre
        # Gauche
        if balle_x - balle_rayon <= 0 :
            balle_vitesse_x = abs(balle_vitesse_x)
            balle_x = balle_rayon
            
            
            if compteur_fautes == 0:
                if compteur_touches_R == 0:
                    score_R += 1
                    faute = "Out B"
                    compteur_fautes = compteur_fautes_max
                    service = 2
                else:  
                    score_B += 1
                    faute = "Out R"
                    compteur_fautes = compteur_fautes_max
                    service = 1
            
            
        # Droite
        if balle_x + balle_rayon >= largeur_fenetre:
            balle_vitesse_x = -abs(balle_vitesse_x)
            balle_x = largeur_fenetre - balle_rayon
            
            if compteur_fautes == 0:
                if compteur_touches_B == 0:
                    score_B += 1
                    faute = "Out R"
                    compteur_fautes = compteur_fautes_max
                    service = 1
                else:
                    score_R += 1
                    faute = "Out B"
                    compteur_fautes = compteur_fautes_max
                    service = 2


        # Haut
        if balle_y - balle_rayon <= 0 :
            balle_vitesse_y = abs(balle_vitesse_y) * frottement
            balle_y = balle_rayon
        
        # Bas
        if balle_y + balle_rayon >= hauteur_fenetre:
            balle_vitesse_y = -abs(balle_vitesse_y)
            balle_y = hauteur_fenetre - balle_rayon
            
            if balle_x > largeur_fenetre//2 :
                
                if compteur_fautes == 0:
                    score_B += 1
                    faute = "Point B"
                    compteur_fautes = compteur_fautes_max
                    service = 1
            
            elif balle_x < largeur_fenetre//2:
                if compteur_fautes == 0:
                    score_R += 1
                    faute = "Point R"
                    compteur_fautes = compteur_fautes_max
                    service = 2
        
        
        # Rebondissement balle sur filet
        if abs(balle_x - filet_x) <= balle_rayon and balle_y >= filet_hauteur - balle_rayon:
            if balle_vitesse_x >= 0:
                balle_x = filet_x - balle_rayon
            else: balle_x = filet_x + balle_rayon
            balle_vitesse_x = - balle_vitesse_x * frottement
    
        
    
     
        # Dessiner la fenêtre
        fenetre.blit(fond, (0, 0))  # Afficher le fond

        # Dessiner le filet
        pygame.draw.rect(fenetre, noir, (filet_x, hauteur_fenetre - filet_hauteur, filet_largeur, filet_hauteur))

        # Dessiner le joueur de gauche    
        if touches_joueur1[pygame.K_z]:
            fenetre.blit(sautG, (joueur1_x, joueur1_y))  # Afficher le saut du joueur de gauche  
        
        elif saut_joueur1 :
            fenetre.blit(frappe1, (joueur1_x, joueur1_y))
            
        elif touches_joueur1[pygame.K_d] :
            fenetre.blit(piedDroit1, (joueur1_x, joueur1_y))
        
        elif touches_joueur1[pygame.K_q]:
            fenetre.blit(piedGauche1, (joueur1_x, joueur1_y))
        
        else :
            fenetre.blit(attente1, (joueur1_x, joueur1_y))  # Afficher la position de base du joueur gauche
    
        # Dessiner le joueur de droite
        if touches_joueur2[pygame.K_UP]:
            fenetre.blit(sautD, (joueur2_x, joueur2_y))  # Afficher le saut du joueur de gauche  
        
        elif saut_joueur2 :
            fenetre.blit(frappe2, (joueur2_x, joueur2_y))
            
        elif touches_joueur2[pygame.K_RIGHT] :
            fenetre.blit(piedDroit2, (joueur2_x, joueur2_y))
        
        elif touches_joueur2[pygame.K_LEFT]:
            fenetre.blit(piedGauche2, (joueur2_x, joueur2_y))
        
        else :
            fenetre.blit(attente2, (joueur2_x, joueur2_y))  # Afficher la position de base du joueur gauche

        # Dessiner la balle
        if balle_vitesse_x < 8 :
            fenetre.blit(balle1, (balle_x, balle_y))
        elif balle_vitesse_x > 8 :
            fenetre.blit(balle2, (balle_x, balle_y))
        elif balle_vitesse_x > 13 :
            fenetre.blit(balle3, (balle_x, balle_y))
        elif balle_vitesse_x > 25 :
            fenetre.blit(balle4, (balle_x, balle_y))
        else :
            fenetre.blit(balle4, (balle_x, balle_y))
        
        
        
        if compteur_fautes != 0 and score_B != 15 and score_R != 15 :
            compteur_fautes -= 1
            
            if faute == "Point B":
                if attaque and compteur_touches_B != 0:
                    fenetre.blit(super_spike_B, (largeur_fenetre // 5, hauteur_fenetre // 4))
                else:  
                    fenetre.blit(point_B, (largeur_fenetre // 5, hauteur_fenetre // 4))
                    
                
            elif faute == "Point R":
                if attaque and compteur_touches_R != 0:
                    fenetre.blit(super_spike_R, (largeur_fenetre // 5 - 10, hauteur_fenetre // 4))
                else:
                    fenetre.blit(point_R, (largeur_fenetre // 5 - 10, hauteur_fenetre // 4))
                    
                
            elif faute == "Out B":
                fenetre.blit(Out_R, (largeur_fenetre // 3 + 30, hauteur_fenetre // 4))
                
            elif faute == "Out R":
                fenetre.blit(Out_B, (largeur_fenetre // 3 + 30, hauteur_fenetre // 4))
                
            elif faute == "4 touches R":
                fenetre.blit(quatre_touches_B, (largeur_fenetre // 5 +30, hauteur_fenetre // 4))
                
            elif faute == "4 touches B":
                fenetre.blit(quatre_touches_R, (largeur_fenetre // 5 +30, hauteur_fenetre // 4))
            
            
            
      
        
        
        if score_B == 0:
            fenetre.blit(zero_B, (50, 50))
        if score_R == 0:
            fenetre.blit(zero_R, (1100, 50))
            
        if score_B == 1:
            fenetre.blit(un_B, (50, 50))
        if score_R == 1:
            fenetre.blit(un_R, (1100, 50))
           
        if score_B == 2:
            fenetre.blit(deux_B, (50, 50))
        if score_R == 2:
            fenetre.blit(deux_R, (1100, 50))
           
        if score_B == 3:
            fenetre.blit(trois_B, (50, 50))
        if score_R == 3:
            fenetre.blit(trois_R, (1100, 50))
            
        if score_B == 4:
            fenetre.blit(quatre_B, (50, 50))
        if score_R == 4:
            fenetre.blit(quatre_R, (1100, 50))
            
        if score_B == 5:
            fenetre.blit(cinq_B, (50, 50))
        if score_R == 5:
            fenetre.blit(cinq_R, (1100, 50))
        
        if score_B == 6:
            fenetre.blit(six_B, (50, 50))
        if score_R == 6:
            fenetre.blit(six_R, (1100, 50))
        
        if score_B == 7:
            fenetre.blit(sept_B, (50, 50))
        if score_R == 7:
            fenetre.blit(sept_R, (1100, 50))
            
        if score_B == 8:
            fenetre.blit(huit_B, (50, 50))
        if score_R == 8:
            fenetre.blit(huit_R, (1100, 50))
        
        if score_B == 9:
            fenetre.blit(neuf_B, (50, 50))
        if score_R == 9:
            fenetre.blit(neuf_R, (1100, 50))
            
        if score_B == 10:
            fenetre.blit(un_B, (25, 50))
            fenetre.blit(zero_B, (50, 50))
        if score_R == 10:
            fenetre.blit(un_R, (1075, 50))
            fenetre.blit(zero_R, (1100, 50))
            
        if score_B == 11:
            fenetre.blit(un_B, (25, 50))
            fenetre.blit(un_B, (50, 50))
        if score_R == 11:
            fenetre.blit(un_R, (1075, 50))
            fenetre.blit(un_R, (1100, 50))
            
        if score_B == 12:
            fenetre.blit(un_B, (25, 50))
            fenetre.blit(deux_B, (50, 50))
        if score_R == 12:
            fenetre.blit(un_R, (1075, 50))
            fenetre.blit(deux_R, (1100, 50))
            
        if score_B == 13:
            fenetre.blit(un_B, (25, 50))
            fenetre.blit(trois_B, (50, 50))
        if score_R == 13:
            fenetre.blit(un_R, (1075, 50))
            fenetre.blit(trois_R, (1100, 50))
            
        if score_B == 14:
            fenetre.blit(un_B, (25, 50))
            fenetre.blit(quatre_B, (50, 50))
        if score_R == 14:
            fenetre.blit(un_R, (1075, 50))
            fenetre.blit(quatre_R, (1100, 50))
            
        if score_B == 15:
            fenetre.blit(un_B, (25, 50))
            fenetre.blit(cinq_B, (50, 50))
            fenetre.blit(victoire_B, (largeur_fenetre // 2 - victoire_B.get_width()//2, hauteur_fenetre // 4))
            compteur_victoire -= 1
        if score_R == 15:
            fenetre.blit(un_R, (1075, 50))
            fenetre.blit(cinq_R, (1100, 50))
            fenetre.blit(victoire_R, (largeur_fenetre // 2 - victoire_R.get_width()//2, hauteur_fenetre // 4))
            compteur_victoire -= 1
            
        if compteur_victoire == 0:
            service = randint(1,2)
            compteur_temps = 650
            compteur_victoire = 200
            score_B = 0
            score_R = 0
            compteur_fautes = 1
            start = False
            
            
        
        # Mettre à jour l'affichage
        pygame.display.flip()
    
        # gestion de la frequence de l'image
        pygame.time.Clock().tick(80)
